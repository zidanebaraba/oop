<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');
$animal = new Animal("Shaun");
echo "Name : " . $animal->name . "<br>";
echo "legs : " . $animal->legs . "<br>";
echo "cold blooded : " . $animal->cold_blooded . "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo $kodok->jump() . "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
echo $sungokong->yell();
